The idea behind the All-Data Storage (ADS) concept is to store the raw
data from all receive paths in LOFAR for a significant (minutes up to
24 hours) amount of time, so that the data can be analysed without
real-time requirements while the raw data remains available for
detailed study of interesting events. If 24 hours of data can be
stored, all standard observations on persistent celestial sources can
be emulated by playing back the raw data.

The aim of this repository is to capture the work done for the
development of this concept.

On March 23, 2022, the Transient Buffer Boards (TBBs) of the Dutch
LOFAR were used to capture 4.2 s of ADC data for all receive paths in
the array. Intern Paul Baecke was the first to inspect this data
set. He checked for failing antennas using spectrograms, resulting in
a list of (seemingly) properly functioning antennas. He also
discovered that the positions of the antennas were not stored
correctly in the HDF5 files in all cases, so he made a list of ETRS
positions as well. These artefacts along with his code and
B.Sc. thesis report can be found in the directory pcbaecke.

In a follow-up project, Vishwapriya Gautam concentrated on enabling
imaging using DP3 and WSClean. To this end, he developed code to
perform either correlation of individual antenna signals or beamform
the stations and correlate the station beam outputs. The results were
converted to a Measurement Set, so that the data could be handled by
DP3 and WSClean. His report and code can be found in the directory
vgautam.


