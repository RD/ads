This directory contains code, report and artefacts produced during
Paul Baecke's internship and B.Sc. project

A short description of files in this repository follows:

BScProject/Praktikumsbericht_Paul_Baecke_TPh19.pdf:
	This is the internship report. It contains more detailed
	descripitions of the imaging and calibration pipeline,  how to
	inspect TBB data and the imple entation of a phase shift as well as PFB.

BScProject/BA_Paul_Baecke_TPh19.pdf : 
	This is the bachelor's thesis. It contains a more detailed
	theoretical background on the needed concepts, pulsars, pulse
	propagation. It contains further information about the LOFAR
	as well as the whole pipeline. A SNR estimation regarding pulsar detectablility with TBB data
	is also included. Furthermore the section discussion and
	outlook summarizes the results of the thesis project and
	provides a possible direction for later projects.

BScProject/srclist3CR.mat:
	This file contains the 3CR, the Third Cambridge Catalogue of Radio Sources

BScProject/Elevation.m:
	A short matlab script which calculates the horizon coordinates
	from celestial coordinates of a given source and time Please
	note, that Ra and Dec, as well as the time of observation have
	been manually typed in for the bachelor thesis project and the
	described dataset

BScProject/Images:
	001000000.png   : Dynamic spectrum of dipole 001000000, as an
	example of a working dipole
	001006051.png   : Dynamic spectrum of dipole 001006051, as an
	example of a "non existing" dipole
	002003029.png   : Dynamic spectrum of dipole 002003029, as an
	example of a flagged dipole
	002010085.png   : Dynamic spectrum of dipole 002010085, as an
	example of a flagged dipole
	Fig_5_[1-4].png : Uncalibrated all-sky images at 59.96MHz for
	a different number of used stations. It has to be mentioned
	that the cardinal directions are not confirmed. The axis
	labeling is representing the image grid.
	Fig_5_1 : CS002. 
	Fig_5_2 : Superterp. 
	Fig_5_3 : 20 core stations.
	Fig_5_4 : 30 dutch stations.
		
	The effect of undersampling can be observed, so the images
	with a high number of used stations are meaningless.  They are
	displayed here to demonstrate this effect

	Fig_8_[1-2].png : Calibrated all-sky images for a different
	number of used stations. It has to be mentioned that the
	cardinal directions are not confirmed. The axis labeling is
	representing the image grid.

	Fig_8_1 : CS002.
	Fig_8_2 : 30 dutch stations.  
	The effect of undersampling can be observed, so the image with
	a high number of used station are meaningless.  They are
	displayed here to demonstrate this effect

Flow_Chart.png: 	
	A flow chart visualizing the created pipeline structure

Flagging/:
	This folder contains the subfunctions responsible for antenna
	flagging. They have been excluded in this way, because the
	flagging happened before the  pipeline was established.

	Flagging_Overview.xlsx:
		This Excel table shows all the dipoles from the
		retrieved data set and whether they have been flagged
		or not.

	Dynamic_Frequency_Spectrum_TBB_CS002.py:
		This script is mainly a remnant from before the
		pipeline was established. However, the concept of
		flagging can be shown as well as its positioning
		within the code. This had been made more clear with
		additional comments, it should be possible to
		implement the relevant code snippets into the regular
		pipeline. The subfunctions which take care of flagging
		are not dependent on this file specifically.

	Find_Divisor.py, Practical_AR.py:
		These subfunctions determine the size of the time
		series matrix. In the case of the retrieved dataset it
		was way to long to just plot it as a dynamic spectrum,
		this is why it had to be compressed. The subfunctions
		return, how much compressing has to be done. They
		might not be generalized, meaning that the
		implementation of these subfunctions for other
		datasets with other durations of observation might not
		work.

	AntennaFlagger.py:
		After one overall dynamic spectrum has been created,
		this function compares the contrast of the dipole's
		dynamic spectrum with the contrast of the overall
		spectrum. If it differs to much, the dipole gets
		flagged
	
	Plot_Spectrogram.py:
		Plots the dynamic spectrum of the dipole and
		saves/displays it

Python/
	This folder contains the main pipeline with calibration, phase
	shifting and imaging. 
	
	Antennas.txt:
		A list of working antennas, to exclude defect dipoles
		from the beginning on.

	Files.txt:
		A list of the file names of each stations HDF5 file

	Position_Exceptions.txt
		A list with the ETRS coordinates of all the dipoles,
		whose coordinates are not in the HDF file. This needs
		some change, ALL dipoles coordinates need to be ETRS,
		given the case that the HDF5 has an error as described
		in the thesis. The ETRS coordinates can be found:
		https://github.com/brentjens/lofar-antenna-positions

	Stations.txt:
		A list of each stations name and the number of not
		defect antennas.
		
	Other files: 
		The pipeline as described in the bachelor's thesis and
		visualized in Flow_Chart.png
		To note: 
		- When imaging all-sky images, adjust the lmgrid so
		that the image does not get undersampled. For more
		than one station the resolution of the image is higher
		than the grid of the image, so it becomes meaningless.
		- When calculating the correlation, you should first
		correlate per subband and then integrate over the ACMs
		and not integrate over the time samples before
		correlation.
		- ITRS and ETRS dipole coordinates are both
		used. Please change it, that only one of the reference
		systems is used. The dipole's ETRS coordinates are:
		https://github.com/brentjens/lofar-antenna-positions

For further questions please contact Prof. Dr. Stefan Wijnholds who
supervised this work: wijnholds@astron.nl

Paul Baecke, 16th October 2022

contact: pbaecke@vodafonemail.de
