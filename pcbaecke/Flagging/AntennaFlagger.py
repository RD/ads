def AntennaFlagger(Dipole_all_data, t, Nrows, NFFT, dset_list):


    # Checks for irregularities and Flags faulty Antennas

	#Input variables:
	#ims	:	Read out matrix of one Dipoles TBB Data. Compressed.
	#t	:	time - length of the measurement
	#Nrows  :	Number of slices in the matrix
	#NFFT	:	Number of elements in Fourier Transformation
	#Sel_Ant:	String of the number of the selected Di
	#Dipole :	counting index resembling the Dipole
	#dset_list:	list of every Dipoles name as string
	#Antcheck	Array containing the names of wos
	#Faulty_Ant:	List of names of faulty Antennas
	#Antperc:	Percentage of the difference between the max value in the ims and the median of the slices

	#return Variables:
	#Faulty_Ant
	#Antcheck
	#Antperc

	# PB, 16 October 2022

	import numpy as np
    from Plot_Spectrogram import Plot_Spectrogram



    # creating the overall dynamic spectrum for all Dipoles
	Dynspec = np.zeros((np.shape(Dipole_all_data[dset_list[0]])))

	Faulty_Ant = []
	Antcheck = []

	print("Creating median dynamic spectrum of whole station")

	for subband in range(len(Dynspec)):
		for time in range(len(Dynspec[0])):
			pixelarr = []

			for Dipole in range(len(dset_list)):

				#directly check for zero all Dipoles to not let them disturb the median too much

				if np.isnan(Dipole_all_data[dset_list[Dipole]][subband, time]) == False:
					pixelarr = np.append(pixelarr, Dipole_all_data[dset_list[Dipole]][subband, time])
				else:
					Faulty_Ant = np.append(Faulty_Ant, dset_list[Dipole])
					break

			pixel = np.median(pixelarr)
			Dynspec[subband, time] = pixel


	# after one overall spectrum has been created, the maximum values get determined to see the variations
	Dipperc = np.zeros(len(dset_list))

	max = np.max(Dynspec)
	Perca = np.zeros(10)
	for idx in range(10):
		Perca = ((np.abs(max - np.median(Dynspec[:, idx * (Nrows//10)])))/((max + np.median(Dynspec[:, idx * Nrows//10]))/2)) * 100

	Perc = np.mean(Perca)

	#	Dipole = Antennalist[Antenna]
	for idx in range(len(dset_list)):

		Sel_Ant = dset_list[idx]
		print("Checking Dipole " + Sel_Ant + " .    " + str(idx+1) + " / " + str(len(dset_list)))
		#finding the maximum value in the matrix to compare the median to it -> contrast checking
		#this is done for 10 columns

		specmat = Dipole_all_data[Sel_Ant]
		max = np.max(specmat)


		percarr = np.zeros(10)
		for jdx in range(10):
			percarr[jdx] =((np.abs(max - np.median(specmat[:, jdx * (Nrows//10)])))/((max + np.median(specmat[:, jdx * Nrows//10]))/2)) * 100


		Dipperc[idx] = np.mean(percarr)


		if Sel_Ant in Faulty_Ant:
			flagind = 2
		elif np.abs(Perc - Dipperc[idx]) < 2:
			Antcheck = np.append(Antcheck, Sel_Ant)
			flagind = 0
		else:
			Faulty_Ant = np.append(Faulty_Ant, Sel_Ant)
			flagind = 1

		# plots the dynamic spectrum
		Plot_Spectrogram(specmat, t, Nrows, NFFT, Sel_Ant, idx, dset_list, flagind)


	return Faulty_Ant, Antcheck



