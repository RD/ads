import h5py
from scipy.fft import fft, fftfreq
import numpy as np
from Find_Divisor import Find_Divisor
from AntennaFlagger import AntennaFlagger

# this script is to show in which point of the script (now Pulsar_Skyimage) the Flagging algorithm can be implemented
# The flagging of the dataset mentioned in the bachelor's thesis happened
# before Pulsar_Skyimage was created, so please excuse this messy way of "implementation"/explanation
# place of implementation: line 108
# PB, 16 October 2022

#(*) -> can be read out of Attributs in .h5 file using HDF5
#create the file
f = h5py.File("/var/scratch/pcbaecke/TBB_Data/L854556_D20220323T211234.762Z_CS002_R000_tbb.h5")

dset_list = ["002000000" , "002000001" , "002000002" , "002000003" ,
                 "002000004" , "002000005" , "002000006" , "002000007" ,
                                             "002001010" , "002001011" ,
                 "002001012" , "002001013" , "002001014" , "002001015" ,
                 "002002016" , "002002017" , "002002018" , "002002019" ,
                 "002002020" , "002002021" , "002002022" , "002002023" ,
                 "002003024" , "002003025" , "002003026" , "002003027" ,
                 "002003028"               , "002003030" , "002003031" ,
                 "002004032" , "002004033" , "002004034" , "002004035" ,
                 "002004036" , "002004037" , "002004038" , "002004039" ,
                                             "002005042" , "002005043" ,
                 "002005044" , "002005045" , "002005046" , "002005047" ,
                 "002006048" , "002006049" , "002006050" , "002006051" ,
                 "002006052" , "002006053" , "002006054" , "002006055" ,
                 "002007056" , "002007057" , "002007058" , "002007059" ,
                                             "002007062" , "002007063" ,
                 "002008064" , "002008065" , "002008066" , "002008067" ,
                 "002008068" , "002008069" , "002008070" , "002008071" ,
                 "002009072" , "002009073" , "002009074" , "002009075" ,
                 "002009076" , "002009077" , "002009078" , "002009079" ,
                 "002010080" , "002010081" , "002010082" , "002010083" ,
                                             "002010086" , "002010087" ,
                 "002011088" , "002011089" , "002011090" , "002011091" ,
                 "002011092" , "002011093" , "002011094" , "002011095" ]


Dipole_all_data = {}

#Antennalist = [0, 1, 2, 4, 5, 8, 9, 29]
#for Antenna in range(len(Antennalist)):
for Dipole in range(10):			#len(dset_list)):
#       Dipole = Antennalist[Antenna]

        #Selected Antenna, in preparation to plot all Antenna Spectrum faster
        Sel_Ant = "/Station002/" + dset_list[Dipole]

        #open dataset (Later on for all the Antennas, probably one big "for" loop
        dset = f[Sel_Ant]

        print("Processing Dipole " + dset_list[Dipole] + ".    " + str(Dipole + 1) + " / " + str(len(dset_list)))

        #show shape
                #print(dset.shape)
        #this showed me what I wanted to see, an array with the length of the Attribute "Data_Length" meaning that the Dataset gets read out correctly

        #Length of Raw Data Array (*)
        Len_Data = int(dset.attrs['DATA_LENGTH'])

        #(*)
        Sample_Frequency = dset.attrs['SAMPLE_FREQUENCY_VALUE'] * 1000000       #Hertz


        #Number of Data points used in each block for the FFT
        #Idea is to chain these along one another to get the dynamic frequency spectrum
        # (*)
        NFFT = int(dset.attrs['SAMPLES_PER_FRAME'])

        #Number of FFT Blocks
        Nblocks = int(Len_Data / NFFT)

        #Sample Spacing
        dt = 1/Sample_Frequency

        t = dt * Len_Data       #s

        #x array from 0 to t with NFFT Samples
        x = np.linspace(0.0, t, Len_Data, endpoint=False)
        y = np.zeros((NFFT//2, Nblocks))


        ###Block wise Fourier Transform

        for idx in range(Nblocks):
                #if (idx+1)%10000 == 0:
                        #print("Calculating FFT " + str(idx+1) + " / " + str(Nblocks))

                x_seg = x[idx*NFFT:(idx*NFFT)+NFFT]
                y_seg = dset[idx*NFFT:(idx*NFFT)+NFFT]

                yf = fft(y_seg)
                xf = fftfreq(NFFT, dt)[:NFFT//2]

                yabs = np.abs(yf[0:NFFT//2])

                y[:, idx] = yabs



        #we now got y
        #this is a matrix with every Fourier transform in a Column each with Nblocks amount of columns

        # this is the placing where the Antenna Flagging can be implemented:
        # right after the retrieval of the full TSM of one Dipole
        # as described in the thesis the flagging happens based of image information of the
        # dynamic spectrum of the dipole

        #Since the AR of the matrix is highly unpractical columns will be combined with intergration
        #Number of Rows which are getting combined
        Nrows = int(Find_Divisor(Nblocks))

        # Number of Elements in each "compressed" row
        Nelem = int(Nblocks / Nrows)

        #New  Matrix of "compressed" Data for better plotting
        spec = np.zeros((NFFT//2, Nrows))

        # this integrates to create a spectrum with a displayable size
        for idx in range(Nrows):
                for jdx in range(NFFT//2):
                        spec[jdx, idx] = np.trapz(y[jdx, idx * Nelem:(idx * Nelem) + Nelem], np.arange(0, Nelem), 1)

        #rescale the absolute value of the spectrogram as rescaling is standard
        ims = 10 * np.log10(spec)

        #creating a dicionary with every Dipole's (dyn spec plotting) data
        Dipole_all_data[dset_list[Dipole]] = ims


print("---Dipole processing complete---")

# after creating the dynamic spectrum in the lines 108-132 now the actual flagging function gets called
# it returns arrays containing the dipole identification numbers of flagged or "okay" dipoles.
Faulty_Ant, Antcheck = AntennaFlagger(Dipole_all_data, t, Nrows, NFFT, dset_list)


for idx in range(len(Antcheck)):
        print("The Antennas " + str(Antcheck[idx]) + " are regular.")
for idx in range(len(Faulty_Ant)):
        print("The Antennas " + str(Faulty_Ant[idx]) + " have been flagged.")



#close objects and files
f.close()

