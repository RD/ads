def Practical_AR(Number, divisors):
	# finds the % 0 divisor which creates a usable image at the end
	#Input Variables:
	#Number	: Number which is to be divided
	#Divisors: List of floor divisors from other subfunction

	#return Variable:
	# prac	:	Divisor which makes sense in terms of creating a visible image

	prac = 0

	for idx in range(len(divisors)):
		if 2000 <= Number / divisors[idx] <= 5000:
			prac = Number / divisors[idx]

	if prac == 0:
		for idx in range(len(divisors)):
			if Number / divisors[idx] <= 2000:
				prac = Number / divisors[idx]
				break
	return prac
