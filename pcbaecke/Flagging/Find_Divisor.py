def Find_Divisor(Number):
	# This function finds Divisor for any number so that Number % x = 0
	# Input
	# Number:	 Number to be divided

	# Return:
	# List of floor divisors.

	import numpy as np

	divisors = []
	for idx in range(1, Number+1):
		if (Number % idx == 0):
			divisors.append(idx)

	from Practical_AR import Practical_AR

	Div = Practical_AR(Number, divisors)


	return Div


