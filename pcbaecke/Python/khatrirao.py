def khatrirao(A, B):

	# computes the Khatri-Rao ( column wise Kronecker) product of A and B

	# arguments
	# A, B  : 2-D matrices with equal second dimension. This is not checked by the function
	#        , so entering matrices with unsuitable dimensions may produce unexpected results

	# return value
	# C : column wise Kronecker product of A and B

	# SJW, 2006
	# transcripted in Python by PB, April 2022

	import numpy as np
	C = np.zeros((len(A) * len(B), len(A[0])), dtype=complex)

	for n in range(len(A[0])):
		C[:, n] = np.kron(A[:, n], B[:, n])

	return C
