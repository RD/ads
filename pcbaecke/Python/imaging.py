import numpy as np
from acm2skyimage import acm2skyimage
from matplotlib import pyplot as plt
from statcal import statcal

def uncalibrated_image(cdata, subband, Dipole_all_ITRF, freq, Beam):

        # calculates, saves or prints out the uncalibrated all-sky image for the chosen subband

        # Arguments:
        # cdata         : matrix containing the complex time series data of the chosen subband
        # subband       : index of the chosen subband
        # Dipole_all_ITRF : ITRF/ETRS (see comments Parallel_Processing) coordinates of dipoles
        # freq          : frequency of the chosen subband
        # Beam          : Variable indicating whether data has been phase shifted or not

        # return values :
        # R             : antenna correlation matrix

        if Beam==False:
                print(f"--- Calculating uncalibrated All-Sky Image without Beamforming ---")
        else:
                print(f"--- Calculating uncalibrated All-Sky Image with Beamforming ---")

        # calculating the ACM
        R = np.conj(np.dot(cdata, np.conj(cdata).T))

        # accx = R[::2, ::2]
        # this is only needed if we want to only analyze one particular polarization

        # creating the grid vector for the image
        # please insure that the image does not get undersampled
        # It might be reasonable to create another function which determines the
        # necessary size of the grid dependent on the number of stations -> maximum baseline length
        # to avoid undersampling
        lmgrid = np.arange(-2, 2.005, 0.005)

        # Calls acm2skyimage which creates the image out of the ACM
        skymapx = acm2skyimage(R, Dipole_all_ITRF[:, 0], Dipole_all_ITRF[:, 1], freq, lmgrid, lmgrid)

        # Plotting the sky image
        # Check whether the cardinal directions and the axes labeling are actually true
        pixel_plot = plt.figure()

        pixel_plot.add_subplot()

        pixel_plot = plt.imshow(skymapx, cmap="afmhot", interpolation="nearest")

        plt.title(f"Uncalibrated Sky Image without Beamforming at {round(freq*1e-6, 2)}MHz ")
        plt.ylabel("South <--m--> North")
        plt.xlabel("East <--l--> West")

        if Beam==False:
                plt.title(f"Uncalibrated Sky Image without Beamforming at {round(freq*1e-6, 2)}MHz ")
                plt.savefig(f"/var/scratch/pcbaecke/TBB_Data/Imaging/Without_Beamforming_{subband}.png")
        else:
                plt.title(f"Uncalibrated Sky Image with Beamforming at {round(freq*1e-6, 2)}MHz ")
                plt.savefig(f"/var/scratch/pcbaecke/TBB_Data/Imaging/With_Beamforming_{subband}.png")

        plt.close()

        return R


def calibrated_image(Nant, R, t_obs, freq, Dipole_all_ITRF, normvec, subband, Beam, Count, subarr):

        # calculates, saves or prints out the calibrated all-sky image for the chosen subband

        # Arguments:
        # Nant          : number of dipoles
        # R             : ACM - antenna correlation matrix
        # t_obs         : time of observation
        # freq          : frequency of the chosen subband
        # Dipole_all_ITRF : ITRF/ETRS (see comments Parallel_Processing) coordinates of dipoles
        # norvec        : vector normal to the stations plane
        # subband       : index of the chosen subband
        # Beam          : Variable indicating whether data has been phase shifted or not
        # Count         : additional indexing variable
        # subarr        : array containg number of all chosen subbands

        if Beam==False:
                print("--- Generating calibrated All-sky Image without Beamforming ---")
        else:
                print("--- Generating calibrated All-Sky Image with Beamforming ---")


        srcsel = [324, 283, 88, 179, 0]                         # A team and the Sun
        bl_restriction = 6                                      # baseline restriction of 10 wavelengths
        max_bl_restriction = 40                                 # max baseline restriction in m
        Nzeros = 0

        # initiation of these arrays
        calx = np.zeros((Nant, 1))
        Sigmanx = np.zeros((Nant-Nzeros, Nant-Nzeros, 1))

        uvmask = np.identity(len(R))

        # starting calibration process
        calx, sigmasx, Sigmanx = statcal(R, t_obs, freq, Dipole_all_ITRF, srcsel, normvec, bl_restriction, max_bl_restriction, uvmask, Count, subarr)

        # creating the grid vector for the image
        # please insure that the image does not get undersampled
        # It might be reasonable to create another function which determines the
        # necessary size of the grid dependent on the number of stations -> maximum baseline length
        # to avoid undersampling
        lmgrid = np.arange(-2, 2.005, 0.005)

        # Calls acm2skyimage which creates the image out of the ACM
        skymapx = acm2skyimage(np.multiply(np.conj(np.dot(calx, calx.T)), R), Dipole_all_ITRF[:, 0], Dipole_all_ITRF[:, 1], freq, lmgrid, lmgrid)

        # plotting the all-sky image
        pixel_plot = plt.figure()

        pixel_plot.add_subplot()

        pixel_plot = plt.imshow(skymapx, cmap="afmhot", interpolation="nearest")

        plt.ylabel("South <--m--> North")
        plt.xlabel("East <--l--> West")

        if Beam==False:
                plt.title(f"Calibrated Sky Image without Beamforming at {round(freq*1e-6, 2)}MHz ")
                plt.savefig(f"/var/scratch/pcbaecke/TBB_Data/Imaging/Without_Beamforming_Calibrated_{subband}.png")
        else:
                plt.title(f"Calibrated Sky Image with Beamforming at {round(freq*1e-6, 2)}MHz ")
                plt.savefig(f"/var/scratch/pcbaecke/TBB_Data/Imaging/With_Beamforming_Calibrated_{subband}.png")

        plt.close()

