def Plot_Spectrogram(ims, t, Nrows, NFFT, Sel_Ant, Dipole, dset_list, flagind):
        ### creating the Spectrogram
	#Input variables:
	#ims    :       Read out matrix of one Dipoles TBB Data. Compressed.
        #t      :       time - length of the measurement
        #Nrows  :       Number of slices in the matrix
        #NFFT   :       Number of elements in Fourier Transformation
        #Sel_Ant:       String of the number of the selected Di
        #Dipole :       counting index resembling the Dipole
        #dset_list:     list of every Dipoles name as string

	#return:
	#This function doesn't return anything, but saves the pictures in given directories. 

	from matplotlib import pyplot as plt
	import numpy as np

	if flagind == 2:
		ims = np.zeros((np.shape(ims)))

	plt.imshow(ims, cmap="inferno")


	xticksnew = np.arange(0, t, 0.5)
	yticksnew = np.arange(0, 100, 10)

	xticksold = np.arange(Nrows)
	yticksold = np.arange(NFFT//2)
	plt.xticks(xticksold[::len(xticksold)//len(xticksnew)+1], xticksnew)
	plt.yticks(yticksold[::-(len(yticksold)//len(yticksnew)+1)], yticksnew)

	plt.ylabel("Frequency in MHz")
	plt.xlabel("Time in s")
	plt.title("Dynamic Spectrum of Antenna " + str(dset_list[Dipole]))

	#plt.show()

	print("Saving picture " + str(Dipole+1) + " of " + str(len(dset_list)))

	'''
	if flagind == 0:
		plt.title("Dynamic Spectrum of Antenna" + Sel_Ant)
		plt.savefig("/var/scratch/pcbaecke/TBB_Data/Unflagged_Antennas/" + dset_list[Dipole] + ".png")
	else:
		plt.title("Dynamic Spectrum of FLAGGED Antenna" + Sel_Ant)
		plt.savefig("/var/scratch/pcbaecke/TBB_Data/Flagged_Antenna_Pics/" + dset_list[Dipole] + ".png")
	'''
	#plt.savefig("/var/scratch/pcbaecke/TBB_Data/All_Spectra/" + dset_list[Dipole] + ".png")
	plt.show()

	plt.close()
