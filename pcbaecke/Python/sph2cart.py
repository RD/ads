def sph2cart(ra, dec, r):

    # this function converts spherical coordinates into cartesian

    # arguments
    # ra    : right ascension
    # dec   : declination
    # r     : radius

    # returned variables
    # x, y, z cartesian coordinates

    import numpy as np
    x = r * np.cos(dec) * np.cos(ra)
    y = r * np.cos(dec) * np.sin(ra)
    z = r * np.sin(dec)

    return x, y, z
