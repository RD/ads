2459662.375def JulianDay(time):

	# Computes the Julian Day (including decimal fraction) corresponding to the requested time

	# arguments
	# time  : UTC time as date number as returned by Matlab: Datenum function

	# Return value
	# JD    : Julian Day number including decimal fraction

	# This function was written by Sebastiaan van der Tol in 2002 to support
	# his ThEA experiments.The conversion formulas were taken from [1].

	# Rererences
	# [1] Peter Duffet - Smith, "Practical Astronomy with Your Calculator",
	# Cambridge University Press, 1979
	#
	# SJW, 2006
	# transcripted in Python by PB, April 2022

	JD = time - 729799.5 + 2450858          # Matlab: datenum(1998, 2, 13, 12, 0, 0) = 729799.5


	return JD
