# this is the first initiating code for the bachelor's thesis project:
# "Performing an all-sky pulsar search with recent raw antenna data of the Low-frequency array" by P. Baecke
# for a more detailed description see the Thesis

# PB, 16 October 2022
# It reads in all the needed stations and working antennas and forwards this to further sub functions
# which are responsible for imaging, calibration and beamforming

# This function also allows to choose between different modes
# Further expansion might directly include the AntennaFlagging or other functions

import numpy as np
from Pulsar_Skyimage import Pulsar_Skyimage
import time

# taking the start time
start = time.perf_counter()


# reads in the list of working antennas into an array
# Antennas.txt is a list of all dipole identification numbers
# All flagged dipoles are excluded from this list
file1 = open("Antennas.txt", "r")
dset_list = file1.readlines()
file1.close()
for idx in range(len(dset_list)):
        dset_list[idx] = dset_list[idx].replace("\n", "")


# reads in the list of  number of working antennas per station
# stations.txt is a list of all station names with the number of unflagged antennas per station
# Station_list is an array containing string values of the number of unflagges antennas per station
file2 = open("Stations.txt", "r")
Station_list = file2.readlines()
file2.close()
for idx in range(len(Station_list)):
        Station_list[idx] = Station_list[idx].replace("\n", "")
        Station_list[idx] = Station_list[idx].replace(Station_list[idx][0:8], "")
        Station_list[idx] = int(Station_list[idx])



# reads in the file names of all 30 stations into an array
# Files.txt contains all file names
file3 = open("Files.txt", "r")
Files_list = file3.readlines()
file3.close()
for idx in range(len(Files_list)):
        Files_list[idx] = Files_list[idx].replace("\n", "")


# Setting up an array for the Stations numbers.
# Since these differ from their name, I've done that manually.
Station_numbers = ["001", "002", "003", "004", "005", "006", "007", "011",
                   "013", "017", "024", "026", "028", "030", "031", "101",
                   "121", "141", "142", "161", "106", "125", "130", "146",
                   "147", "150", "166", "167", "169", "189"]


# Setting up configurations (CS002, Terp, Core, NL)
# this can be done by manual input when the program runs, or put in directly in the code if wanted
Conf = input("Please enter how many stations you want to use:"
             " \n Type 1 for CS002 \n Type 2 for Superterp \n"
             " Type 3 for Core Stations \n Type 4 for all NL Stations \n")

# configurations
if Conf == "1":
        # CS002
        dset_positions = dset_list[Station_list[0]:Station_list[0]+Station_list[1]]
        Files = [Files_list[1]]
        Station_numbers = [Station_numbers[1]]
        Station_list = [Station_list[1]]
elif Conf == "2":
        # Superterp
        dset_positions = dset_list[Station_list[0]: np.sum(Station_list[0:7])]
        Files = Files_list[1:7]
        Station_numbers = Station_numbers[1:7]
        Station_list = Station_list[1:7]
elif Conf == "3":
        # Core stations
        dset_positions = dset_list[0: np.sum(Station_list[0:20])]
        Files = Files_list[0:20]
        Station_numbers = Station_numbers[0:20]
        Station_list = Station_list[0:20]
elif Conf == "4":
        # LOFAR_NL (Only the 30 of 38 stations that have been retrieved from the LTA)
        dset_positions = dset_list
        Files = Files_list

# Choice of subbands for beamforming
# this could be implemented in another configuration format
# an automatic SNR estimation with automatic selection of the number and frequency of the
# needed subbands might be also possible
# please take computational limits into account
subarr = [306, 307, 308]


Pulsar_Skyimage(dset_positions, Files, Station_list, Station_numbers, start, subarr)
