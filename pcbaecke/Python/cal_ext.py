def cal_ext(acc, A, sigmas, mask, diffstop, maxiter):

	# array calibration assuming the model

	# R = G * A * Sigmas_s * A' * G' + Sigmas_n

	# where
	# G : diagonal Nelem x Nelem gain matrix with complex receiver gains on the main diag., assumed unknown
	# A : Nelem x Nsrc matrix describing the geometrical delays, assumed known
	# Sigma_s   : diagonal Nsrc x Nsrc matrix with apparent source powers on the main diagonal, assumed unknown
	# Sigma_n   : Nelem x Nelem noise covariance matrix, which may have a number of non-zero (off-diagonal)
	#           elements, which are assumed unknown

	# The calibration is done using the weighted alternating least squares method described in [1] with extension
	# to non-zero noise covariance matrix as described in [2]

	# arguments
	# acc   : Nelem x Nelem measured array covariance matrix
	# A     : Nelem x Nrsc known geometrical delays
	# sigmas: Nsrc x 1 vector with initial source power estimates
	# mask  : Nelem x Nelem Matrix with ones marking the entries for which the noise covariance matrix
	#         should be estimated and zeros elsewhere
	# diffstop  : optional argument defining the stop criterion based on the difference between
	#             the solution vectors found in consecutive iteration, default value is 1e-10
	# maxiter   : optional argument defining the maximum number of iterations, default value is 10

	# return values
	# g     : Nelem x 1 vector with estimated complex receiver gains
	# sigmas: Nsrc x 1 vector with estimated apparent source powers -> flux vector
	# Sigma_n:Nelem x Nelem estimated noise covariance matrix

	# References
	# [1] Stefan J. Wijnholds and Alle-Jan van der Veen, "Multisource Self-calibration for Sensor Arrays"
	# IEEE Transactions on Signal Processing, V57, no. 9, pp3512-3522. September 2009
	# [2] Stefan J. Wijnholds and Alle-Jan van der Veen, "Self-Calibration of Radio Astronomical Arrays With
	# Non Diagonal Noise Covariance Matrix", 17th European Signal Processing Conference, August 24-28, 2009, Glasgow

	# SJW, 16 June 2010
	# transcripted in Python by PB, April 2022

	import numpy as np

	# parameters
	Nelem = len(A)
	Nsrc = len(A[0])


	'''
	if (nargin > 4)
		diffstop = varargin
		{1};
	else
		diffstop = 1e-10;
		end
	if (nargin > 5)
		maxiter = varargin
		{2};
	else
		maxiter = 10;
		end

	# since we have 5 arguments imma just jump ahead and assumed:
	maxiter = 10
	'''


	# initialization
	ghat = np.zeros((Nelem, maxiter+1), dtype=complex)
	ghat[:, 0] = 1
	sigmahat = np.zeros((Nsrc, maxiter + 1))
	# this also could be done more efficient probably
	for idx in range(Nsrc):
		sigmahat[idx, 0] = sigmas[idx]
	Sigma_n = np.zeros(Nelem)

	iterat = 0

	# implementation using WALS
	for idx in range(1, maxiter + 1):                # I've used idx insted of iter, since iter seems to be preoccupied
		# original code from previous matlab version, left out since it's not used anyway

		from gainsolv import gainsolv

		# this matrix is precalculated for clarity's sake
		mat11 = np.dot(np.dot(A, np.diag(sigmahat[:, idx-1])), np.conj(np.transpose(A)))
		mat12 = (np.ones((mask.shape)) - mask)
		mat1 = np.multiply(mat11, mat12)
		mat2 = np.multiply(acc, mat12)

		#ITER = idx + 1
		# gain estimation using StefCal - experimental
		ghat[:, idx:idx+1] = gainsolv(1e-6, mat1, mat2, ghat[:, idx-1])
		# This function makes "points out of blobs"


		# some elements of ghat are infinite. Pythons warning for div by 0 seems to be taken into acc in the Matlab code

		GA = np.dot(np.diag(ghat[:, idx]), A)
		Rest = np.dot(np.dot(GA, np.diag(sigmahat[:, idx - 1])), np.conj(np.transpose(GA)))
		compidx = mat12!=0
		# I choose to make the vectors manually, since Rest[compidx] creates 1 dim array, manually 2dim

		Restcomp = Rest[compidx].reshape(len(Rest[compidx]), 1)
		acccomp = acc[compidx].reshape(len(acc[compidx]), 1)

		#if idx == 2:
		#    print(idx)
		#    print(ghat[:, idx])
		#    print(GA)
		#    print(Restcomp)

		normg = abs(np.sqrt(np.dot(np.linalg.pinv(Restcomp), acccomp)))
		# the normg is deviating in the 7th decimal in comparison to Matlab. I evaluate this as beeing sufficient enough
		# I don't know the reason for that though
		# print('normg is: ' + str(normg))

		ghat[:, idx] = normg * ghat[:, idx] / (ghat[0, idx] / abs(ghat[0, idx]))

		# when some values are infinite, they are going to be set 1
		# I do it also not most efficiently to obtain understanding of whats happening
		ghatinfinity = np.isfinite(ghat[:, idx])
		for jdx in range(len(ghat)):
			if ghatinfinity[jdx] == 0:
			        ghat[jdx, idx] = 1

		# estimate sigmahat using sigmanhat and ghat
		invR = np.linalg.inv(acc)
		GA = np.dot(np.diag(ghat[:, idx]), A)

		invabsmat = (np.linalg.inv(np.square(abs(np.dot(np.dot(np.conj(np.transpose(GA)), invR), GA)))))
		diagmultmat = np.diag(np.dot(np.dot(np.dot(np.dot(np.conj(np.transpose(GA)), invR), (acc - Sigma_n)), invR), GA))



		sigmahat[:, idx] = (np.dot(invabsmat, diagmultmat)).real

		# this checks again for infinities in sigmahat
		if sum(np.isinf(sigmahat[:, idx])) != 0:
			sigmahat[:, idx] = sigmahat[:, idx-1]

		# use fiest sourve as amplitude reference
		if (sigmahat[0, idx] != 0):
			sigmahat[:, idx] = sigmahat[:, idx] / sigmahat[0, idx]

		# remove negative values
		sigmahat[sigmahat<0] = 0


		# estimate siganhat using sigmahat and ghat
		Sigma_n = np.multiply((acc - np.diag(np.dot(sigmahat[:, idx], np.conj(np.transpose(GA))))), mask)

		# test for convergence
		theta_prev = np.append(np.array(ghat[:, idx-1]), sigmahat[:, idx-1])
		theta_prev = theta_prev.reshape(len(theta_prev), 1)

		theta = np.append(np.array(ghat[:, idx]), sigmahat[:, idx])
		theta = theta.reshape(len(theta), 1)

		if (abs(np.dot(np.linalg.pinv(theta_prev), theta) - 1)) < diffstop:
			iterat = idx
			break

	print('optimization stopped after ' + str(iterat + 1) + ' iterations.')
	g = ghat[:, iterat]
	sigmas = sigmahat[:, iterat]


	return g, sigmas, Sigma_n
