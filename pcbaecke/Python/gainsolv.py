def gainsolv(tolopt, R0, Rhat, g0):

	# gain estimation using algorithm developed by Stef Salvini, edited by SJW for readability

	# Arguments:
	# tolopt    : stop criterion on the change in the norm of the estimated gain vectors in consecutive iterations
	# R0        : Nelem x Nelem measured array covariance matrix, zero entries are flagged
	# g0        : Nelem x 1 vector with initial estimate for gains

	# return values
	# g         : Nelem x 1 estimated gain vector

	# SS & SJW, March 7, 2012
	# transcripted in Python by PB, April 2022

	# initialization

	import numpy as np

	Nelem = len(g0)
	g = g0
	colnorm = np.zeros((Nelem, 1), dtype=complex)
	Rhatnorm = Rhat

	# normalize the measured array covariance matrix (weighting)
	for idx in range(Nelem):
		colnorm[idx] = np.dot(np.conj(np.transpose(Rhat[:, idx])), Rhat[:, idx])
		Rhatnorm[:, idx] = (Rhat[:, idx] / colnorm[idx])


	# initial calibration
	Rcolcal = np.zeros((Nelem, Nelem), dtype=complex)


	for idx in range(Nelem):
		Rcolcal[:, idx] = np.multiply(g, R0[:, idx])

	# start iterating
	calest = np.zeros((Nelem, 1), dtype=complex)
	gresult = np.zeros((Nelem, 800), dtype=complex)


	for idx in range(800):
		# update gain estimate
		for jdx in range(Nelem):
			calest[jdx] = np.dot((np.conj(np.transpose(Rhatnorm[:, jdx]))), (Rcolcal[:, jdx]))

		g = np.divide(np.ones(calest.shape), np.conj(calest))

		if (idx+1) % 2 > 0:
			# find new convergence curve
			gest1 = g

		else:
			# update calibration results so far and check for convergence
			gold = g
			g = (g + gest1) / 2
			if (idx + 1) >= 2:
				dgnorm = np.linalg.norm((g - gold), ord=2)
				gnorm  = np.linalg.norm(g, ord=2)

				if ((dgnorm / gnorm) <= tolopt):
					# print('convergence reached after ' + str(idx) + ' iterations')
					# print('relative change in norm(g) in the last iteration: ' + str(dgnorm / gnorm))

					break
		# update calibration of array covariance matrix
		for jdx in range(Nelem):
			Rcolcal[:, jdx:jdx+1] = np.multiply(g, R0[:, jdx:jdx+1])




		gresult[:, idx:idx] = g



	return g

	# save gainiter gresult ??

