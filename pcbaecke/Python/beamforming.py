from math import pi
import numpy as np



def beamforming_data(subband, freqarr, Nblocks, cdata, dset_list, t_del):

        # this function applies the phase shift to the complex time series data for one subband

        # arguments:
        # subband       : number of chosen subband
        # freqarr       : array of frequencies per subband
        # Nblocks       : Length of the TSM
        # cdata         : array containing complex time series matrix of the dipole, NFFT x Nblocks
        # dset_list     : Array containing strings with dipole identification numbers
        # t_del         : array containing the time delay on which the wave arrives at the dipoles

        # return values:
        # cdata         : complex time series data with implemented phase shift

        # PB, 16 October 2022


        omega = 2 * pi * freqarr[subband]

        phi = omega * t_del

        # with phi the phasor for each antennas can be calculated
        # https://de.wikipedia.org/wiki/Phasor
        # here the (exp(i*phi) gets written down as cos(x) + i*sin(x)

        phasor = np.zeros(np.shape(phi), dtype=complex)
        for idx in range(len(phi)):
                phasor[idx] = complex(np.cos(phi[idx]), np.sin(phi[idx]))

        Beamformed_data = np.zeros((len(dset_list), Nblocks), dtype = complex)

        print(f"--- Applying Beamforming phasors to Subband {subband+1} --- ")

        Beamformed_data = cdata.T * phasor
        cdata = np.transpose(Beamformed_data)

        return cdata
