def acm2skyimage(acm, xpos, ypos, freq, l, m):

	# conversion of ACM to sky image

	# arguments
	# acm   : Nelem x Nelem x Nchannel array correlation matrix -> accx
	# xpos  : x-position of antennas (vector of length Nelem) -> antpos[1, :]
	# ypos  : y- "~"  -> antpos[2, :]
	# freq  : frequencies in Hz (vector of length channel)
	# l, m  : l and m points to which to direct the array (vectors) -> lmgrid

	# return value
	# skymap : length(l) x length(m) x channel matrix containing the resulting sky maps

	# SJW, 2004
	# modified April 19, 2005 by SJW: correct coordnate conventions
	# modified July 20, 2006 by SJW: optimization for release in LOFAR package
	# transcripted in Python by PB, April 2022

        import numpy as np
        from math import pi, exp, sin, cos
        import cmath


        c = 2.997924e8      # speed of light

        Nelem = len(acm)
        nchannel = 1

	# Every channel has its place in the antenna correlation matrix

        skymap = np.zeros((len(l), len(m), nchannel))
	# creating our 'white empty canvas'
	# for the current objective l=m, but that could change for other aspect ratios of the wanted image


        for nch in range(nchannel):
		#print('acm2skyimage: working on channel' + str(nch+1) + ' of ' + str(nchannel))
		#print("The frequency of this subband is: " + str(freq))
                lamba = c / freq        # lambda is a preoccupied function in Python, hence the spelling

        #print("Lambda: " + str(lamba))
        # wavelength
        # since our freq here is for one subband and no array, there's no index
        k = (2 * pi) / lamba
        # wavevector

        wx = np.exp(-1j * k * np.outer(xpos, l))
        wy = np.exp(-1j * k * np.outer(ypos, m))

        #print("np.shape(wx): " + str(np.shape(wx)))
        #print("np.shape(wy): " + str(np.shape(wy)))
        #print("np.shape(acm) -> np.shape(R): " + str(np.shape(acm)))
        # wavefunction on x and y axis distributed over the grid
        # since I cannot put a matrix in an exponent in Python I tried to work around it a little


        # the following for loops create the image pixel by pixel
        # every pixel gets weighted with the given value
        skymap = np.zeros((len(l), len(m)))
        for lidx in range(len(l)):
                for midx in range(len(m)):
                        weight = np.multiply(wx[:, lidx], wy[:, midx])
                        skymap[lidx, midx] = (np.dot(np.dot(np.conj(weight).T, acm), weight)).real

        return skymap
