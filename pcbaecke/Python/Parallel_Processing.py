import h5py
from scipy.fft import fft, fftfreq
import numpy as np


# this subfunction calculates the FFTs which calculates
# the raw antenna data into complex time series data

# arguments:
# Station_list  : Array containing strings with values of the number of unflagges antennas per station
# Station_numbers:Array containing strings with each stations identification number
# Station       : Number of the current station (for loop in Pulsar_Skyimage)
# dset_list     : Array containing strings with dipole identification numbers
# Antenna_Count : Value how many dipoles have been processed
# cdata         : Empty array, returns complex time series data
# subarr        : Array containing the subbands for calculation
# file          : Array containing strings with HDF5 file names
# Dipole        : Index of the current Dipole within one station
# Exception_count, Exception : to be changed

# return values:
# cdata         : array containing complex time series matrix of the dipole, NFFT x Nblocks
# NFFT          : Number of elements in the FFT
# Dipole_ITRF   : ITRF coordinate of the dipole
# Nblocks       : Length of the TSM
# Dipole        : Index of the current Dipole within one station
# Exception     : To be changed

# PB, 16 October 2022


def Parallel_Processing(Station_list, Station_numbers, Station, dset_list, Antenna_Count, cdata, subarr, file, Dipole, Exception_Count, Exception):

        # Open the h5 file, h5py file cannot be pickled, thats why it happens here
        F = h5py.File("/var/scratch/wijnhold/ADS_20220323/" + file[Station])

        # directory within the HDF5 for the dipole's dataset
        Sel_Ant = f"/Station{Station_numbers[Station]}/{dset_list[Antenna_Count + Dipole]}/"

        #open dataset (Later one for all the Antennas, probably one big "for" loop
        dset = F[Sel_Ant]

        #reading out the dipoles ITRF coordinates
        Dipole_ITRF = dset.attrs["ANTENNA_POSITION_VALUE"]

        # For some stations the are no proper coordinates within the HDF5 file
        # Thats why this element detects whether this is one of those cases
        # It reads out the ETRS coordinates of those dipoles from the handmade
        # list Position_Exceptions.txt
        # IMPORTANT: This is not correct the way it happens.
        # For some stations ITRF coordinates are used for others ETRS
        # It has to changed in a way that all coordinates are one or the other system
        # The coordinates of the dipoles can be retrieved from:
        # https://github.com/brentjens/lofar-antenna-positions
        if Dipole_ITRF[0]==0:

                Exception = True

                with open("Position_Exceptions.txt") as f:
                        lines = f.readlines()

                line = lines[Exception_Count + Dipole -1]
                line = line[4:]

                for idx in range(3):

                        Dipole_ITRF[idx] = line[:10]
                        line = line[12:]

        print(f"Processing Dipole {dset_list[Antenna_Count + Dipole]} --- {Antenna_Count + Dipole +1} / {len(dset_list)}")

        #Length of Raw Data Array
        Len_Data = int(dset.attrs['DATA_LENGTH'])

        Sample_Frequency = dset.attrs['SAMPLE_FREQUENCY_VALUE'] * 1000000       #Hertz

        #Number of Data points used in each block for the FFT
        #Idea is to chain these along one another to get the dynamic frequency spectrum
        NFFT = int(dset.attrs['SAMPLES_PER_FRAME'])

        #Number of FFT Blocks
        Nblocks = int(Len_Data / NFFT)

        #Sample Spacing
        dt = 1/Sample_Frequency

        # duration of observation
        t = dt * Len_Data       #s

        #x array from 0 to t with NFFT Samples
        #if Dipole == 0:
        x = np.linspace(0.0, t, Len_Data, endpoint=False)
        y = np.zeros((NFFT//2, Nblocks), dtype=complex)

        ###Block wise Fourier Transform
        for idx in range(Nblocks):
                x_seg = x[(idx)*NFFT:((idx)*NFFT)+NFFT]
                y_seg = dset[(idx)*NFFT:((idx)*NFFT)+NFFT]

                yf = fft(y_seg)
                xf = fftfreq(NFFT, dt)[:NFFT//2]

                yf = (yf[0:NFFT//2])
                y[:, (idx)] = yf

        #we now got y
        #this is a matrix with every Fourier transform in a Column each with Nblocks amount of columns

        # Creating the cdata array only forwarding the complex time series data of the wanted subbands
        for subband in subarr:
                cdata = np.append(cdata, y[subband, :])

        y = 0

        # closing the HDF5 file
        F.close()

        return cdata, NFFT, Dipole_ITRF, Nblocks, Dipole, Exception


