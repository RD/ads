This directory contains code, report produced during
Vishwapriya Gautam's research internship 

A short description of files in this repository follows:

gautam_report.pdf:
	This is the internship report. It contains more detailed 
	desctiptions of the project along with required theoretical
	background and the developed imaging pipepline.

stat_beamformer1.m:
	This is the matlab code that performs tied-array beamforming
	and alo verifies it with the antenna-based visibilities all the
	way from loading the antenna configurations of the superterp st-
	-ations to performing calibration and then beamforming.

beamforming_pycode:
	It is self sufficient folder that contains python codes to 
	perform beamforming from the six superterp stations. It is 
	particularly interesting that it has used a module named 
	antpos that contains information aboout the location of 
	antenna positions and hence it does not need any files -
	-specifically for LOFAR antenna/stations location info.
	
create_ms_file:
	This folder contains files and jupyter notebooks related
	 to creation of measurement set(.ms) file. 
	/a12-SB212-1sec-reference.ms.zip:
		This is the ms file that has been used as a reference for creating
		other measurement set file.
	/tbb_dat19.ms.zip:
		This is the ms file that has been created using Create MS file 1.ipynb

	/Create MS file 1.ipynb:
		This is the jupyter notebook used to create tbb_dat19.ms
		The final steps for creating the measurement set include
		--parset = dp3.parameterset.ParameterSet()

		--parset.add("msin", "tbb_dat19.ms")
		--parset.add("msout", "tbb_dat19_out.ms")
		--parset.add("steps", "[]")
		--parset.add("out.overwrite", "True") 

		--first_step = dp3.make_main_steps(parset)

		--first_step.set_info(dp3.DPInfo())
		--first_step.process(dp3.DPBuffer()) 

		The code written after the above commands are for inspection
		 and experimentation to fill some fields.

	/Taql_msfile_commands.ipynb
		This jupyter notebook contains scripts that can be used for getting 
		a feel for the taql commands and how it can be used in association with
		measurement set file to visualize the contents and fill in the fields.


For further questions please contact Prof. Dr. Stefan Wijnholds who
supervised this work: wijnholds@astron.nl

Vishwapriya Gautam, 12 November 2023

contact: vishwapriyagautam@tum.de
