import numpy as np
import matplotlib.pyplot as plt
import lofarantpos.db as ldb
import numpy.linalg.linalg as linalg

statname = ['CS002LBA', 'CS003LBA', 'CS004LBA', 'CS005LBA',
             'CS006LBA', 'CS007LBA']
#list of station names can be modified to include other stations 
db = ldb.LofarAntennaDatabase()

def get_phs_centers(statname):
    """ (list of station names) -> (array of coordinates)
    
    get the station centers in local horizon coordinate system wrt CS002 phase center."""
    rotmat = db.pqr_to_etrs["CS002LBA"].T# since superterp stations have same rotation matrix.

    stref = []
    for st in statname:
        strefi = db.phase_centres[st]
        stref.append(strefi)
    stref = np.array(stref)
    stref_loc = stref - np.tile(stref[0], (6, 1))

    stref_loc =  np.linalg.solve(rotmat.T, stref_loc.conj().T).conj().T

    return stref_loc
    
def get_locAntpos(statname):
    """(list of station names) -> (array of ITRF coordinate systems)
    
    get the antenna postions in local coordinates for the stations intended for beamforming
    """

    rotmat = db.pqr_to_etrs["CS002LBA"].T #since superterp stations have same rotmat
    Nstat = len(statname)
    Nant = 48 
    posITRF = np.zeros((Nant*Nstat, 3))
    for i in range(Nstat):
        antposi = db.antenna_etrs(statname[i])[Nant:]
        posITRF[48*i:48*(i+1),:]= antposi - db.phase_centres[statname[i]]

    posITRF = np.array(posITRF)
    Antpos_loc= np.linalg.solve(rotmat.T, posITRF.conj().T).conj().T

    return Antpos_loc









        

