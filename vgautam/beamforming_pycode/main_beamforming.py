import numpy as np
from scipy.io import loadmat 
import matplotlib.pyplot as plt 
import antpos
import beamformed_image

# load data and source location
superterp_data = loadmat('superterp_data.mat')
fobs = superterp_data["fobs"]
fobs = np.array(fobs).flatten()
fobs = fobs[0]

covmat= superterp_data["covmat"]


srcloc= loadmat("srcloc.mat")
srcloc=np.array(srcloc['srcloc'])
#srcloc= np.transpose(np.array(srcloc)) # now shape is (3, 1)

# get antenna positions 
statname = ['CS002LBA', 'CS003LBA', 'CS004LBA', 'CS005LBA',
             'CS006LBA', 'CS007LBA']
Nstat = len(statname)

stat_ref_loc = antpos.get_phs_centers(statname)

antpos_loc = antpos.get_locAntpos(statname)

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.scatter(antpos_loc[:,0], antpos_loc[:,1], s=10, c='b', marker="s", label='fig 1')
plt.xlabel('West ---> East')
plt.ylabel('South --> North')
plt.legend(loc='upper left')
plt.show()

R_stations = beamformed_image.Beamformed_acm(covmat, srcloc, antpos_loc, Nstat, fobs)

lsrc=float(srcloc[0,0])
msrc=float(srcloc[0,1])
nsrc=float(srcloc[0,2])

l=np.linspace(lsrc+0.1,lsrc+0.2,101)
m=np.linspace(msrc+0.1,msrc+0.2,101)

xpos = stat_ref_loc[:, 0]
ypos = stat_ref_loc[:, 1]


sky_map = beamformed_image.acm2skyimage(R_stations, xpos, ypos, fobs, l, m)

plt.figure(2)
plt.title('Beamformed Image')
plt.imshow(sky_map, origin = "upper", extent=[lsrc-0.1,lsrc+0.1,msrc-0.1,msrc+0.1], label = 'Beamformed Image')
plt.xlabel('West ---> East')
plt.ylabel('South --> North')
plt.legend(loc = 'best')
plt.colorbar()
plt.show()