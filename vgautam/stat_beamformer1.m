% clear workspace
clear
close all

load srclist3CR.mat

% load antenna configuration
statname = {'CS002', 'CS003', 'CS004', 'CS005', 'CS006', 'CS007'};
Nstat = length(statname);
Nant = 48;
posITRFabs = zeros(Nstat * Nant, 3);
pos_stat = zeros(Nstat * Nant, 3);

%VPGline1 added stat_ref to store station centers
stat_ref = zeros(6,3);
for statidx = 1:Nstat
    if statidx == 1
        %[poslocal, posITRF, lon, lat, refpos, rotmat] = parseAntennaField(['../AntennaFields/' statname{statidx} '-AntennaField.conf'], 2, 0);
        %[poslocal, posITRF, lon, lat, refpos, rotmat] = parseAntennaField(['~/data-reduction/ADS/AntennaFields/' statname{statidx} '-AntennaField.conf'], 2, 0);
        [poslocal, posITRF, lon, lat, refpos, rotmat] = parseAntennaField([statname{statidx} '-AntennaField.conf'], 2, 0);
        refpos002 = refpos;
        %VPGline2 added for storing station center
        stat_ref(statidx,:)=refpos;
    else
        %[poslocal, posITRF, ~, ~, refpos, ~] = parseAntennaField(['../AntennaFields/' statname{statidx} '-AntennaField.conf'], 2, 0);
        [poslocal, posITRF, ~, ~, refpos, ~] = parseAntennaField([statname{statidx} '-AntennaField.conf'], 2, 0);
        %VPGline 3 added for storing station center
        stat_ref(statidx,:)=refpos;
    end
    posITRFabs((statidx - 1) * Nant + 1:statidx * Nant, :) = posITRF + repmat(refpos.', [Nant, 1]);
    pos_stat((statidx - 1) * Nant + 1:statidx * Nant, :) = poslocal;
end
posxyz = (posITRFabs - repmat(refpos002.', [Nstat * Nant, 1])) / rotmat;

% use A-team and Sun as source
srcsel= [324, 283, 88, 179, 0];
t_obs = datenum(2022, 3, 23, 21, 12, 34);%time of observation
if (sum(srcsel == 0) ~= 0)
    [raSun, decSun] = SunRaDec(JulianDay(t_obs));          % J2000
    rasrc = [srclist3CR(srcsel(srcsel ~= 0)).alpha, raSun].';   % B1950
    decsrc = [srclist3CR(srcsel(srcsel ~= 0)).delta, decSun].'; % B1950
    epoch = [true(length(srcsel(srcsel ~= 0)), 1); false];
else
    rasrc = [srclist3CR(srcsel(srcsel ~= 0)).alpha].';   % B1950
    decsrc = [srclist3CR(srcsel(srcsel ~= 0)).delta].'; % B1950
    epoch = [true(length(srcsel(srcsel ~= 0)), 1)];
end

%% load ACC
Nsb = 300;
R = zeros(Nstat * Nant, Nstat * Nant, Nsb);
for statidx1 = 1:Nstat
    for statidx2 = statidx1:Nstat
        filename = ['superterp/ACC_' statname{statidx1}(3:end) 'X_' statname{statidx2}(3:end) 'X.mat'];
        load(filename)
        for sbidx = 1:Nsb
            R((statidx1 - 1) * Nant+1:statidx1 * Nant, (statidx2 - 1) * Nant + 1: statidx2 * Nant, sbidx) = acc(sbidx, :, :);
        end
        if statidx1 ~= statidx2
            for sbidx = 1:Nsb
                R((statidx2 - 1) * Nant+1:statidx2 * Nant, (statidx1 - 1) * Nant + 1: statidx1 * Nant, sbidx) = squeeze(acc(sbidx, :, :))';
            end
        end
    end
end
%%VPG for making correct images after calibration without shift
%%in ADS_superterp_analysis.m on 28.06.2023 

R = conj(R);

%define frequency
% SJW: if sbsel is the Python index, shouldn't this be +1?
sbsel = sbsel - 1; % to compensate for Python counting from 0 and Matlab counting from 1
freq = 0:1e8/512:1e8;
fsel = freq(sbsel);
sbidx = 250;


srcpos = radectoITRF(rasrc, decsrc, epoch, JulianDay(t_obs));
%srcloc is the source location towards which the beam has to look.
%it is the location of Cas A

% SJW: in calculating the BF delays, you seem to use the reference position
% of CS002 for all antennas. This has implications for the beamforming
% later on as beamforming towards the reference position can be done
% without delay! I have modified the code to operate per station in local
% horizon coordinate system
[lsrc, msrc] = radectolm(rasrc(1), decsrc(1), JulianDay(t_obs), lon, lat)
% experiment to point the station beam off-source
%lsrc = lsrc - 0.1
srcloc = [lsrc, msrc, sqrt(1 - lsrc^2 - msrc^2)];
c = 2.9979245e8;

%try Beamforming

R_bf = zeros(6,6);
for i=1:6
    for j=1:6
        %stat1 = posxyz(1+(i-1)*48:(i*48),:);
        stat1=pos_stat(1+(i-1)*48:(i*48),:);
        tau1=(stat1*srcloc');
        % SJW: added minus sign
        phs_tau1=exp(-2*pi*1i*fsel(sbidx)*tau1/c);
        
        %stat2=posxyz(1+(j-1)*48:(j*48),:);
        stat2=pos_stat(1+(j-1)*48:(j*48),:);
        tau2=(stat2*srcloc');
        % SJW: added minus sign
        phs_tau2=exp(-2*pi*1i*fsel(sbidx)*tau2/c);
        R_bf(i,j) = (phs_tau1'*R(1+(i-1)*48:(i*48), 1+(j-1)*48:(j*48),sbidx))*phs_tau2;
    end
end

% SJW: center image on pointing direction
l = lsrc-0.1:0.002:lsrc+0.1;
m = msrc-0.1:0.002:msrc+0.1;
sky_map = zeros(length(l), length(m));
lambda = c/fsel(sbidx);
k = 2*pi/lambda;
% SJW: Note that stat_ref is specified in ITRF and not in the local horizon
% system, while (l, m) coordinates are in the local horizon system. I
% corrected this by rotating the station reference position to the local
% horizon system of the superterp.
stat_ref_local = (stat_ref - repmat(refpos002.', [6, 1])) / rotmat;

%make skymap from the obtained beamformed covmat
% SJW: use acm2skyimage
sky_map = acm2skyimage(R_bf, stat_ref_local(:, 1), stat_ref_local(:, 2), fsel(sbidx), l, m);
% SJW: use pcolor to plot with proper axis labeling
figure
pcolor(l, m, 10*log10((1/max(sky_map(:)))*sky_map))
shading flat
title('Beamformed Image towards Cas A no cal')
colorbar

%R = conj(R); %%%28.06.2023 VPG done to see the affect on calibrated beamforming
%% attempt calibration
% collect indices for failing antennas
bad_ant_xpol = [];
bad_ant_ypol = [];
for statidx = 1:Nstat
    fid = fopen(['Flagged RCUs/flagged_RCUs_' statname{statidx} '.csv'], 'r')
    temp = textscan(fid, '%s', 'Delimiter', ';');
    Nflags = length(temp{1}) / 2;
    temp = reshape(temp{1}, [2, Nflags]).';
    for flagidx = 1:Nflags
        RCUid = str2num(temp{flagidx, 2}(7:9));
        if mod(RCUid, 2) == 0
            bad_ant_xpol = [bad_ant_xpol, RCUid / 2 + 1 + (statidx - 1) * Nant];
        else
            bad_ant_ypol = [bad_ant_ypol, (RCUid + 1) / 2 + (statidx - 1) * Nant];
        end
    end
end
bad_ant_idx = bad_ant_xpol;
antsel = true(Nstat * Nant, 1);
antsel(bad_ant_idx) = false;
t_obs = datenum(2022, 3, 23, 21, 12, 34);
% use A-team and Sun for calibration
srcsel = [324, 283, 88, 179, 0];
% normal vector to station, would be nice to get this from
% parseAntennaField
normal = [0.598753; 0.072099; 0.797682];
cal = zeros(Nant * Nsb, Nsb);
for sbidx = sbidx % 1:Nsb 
    disp(['working on frequency ' num2str(fsel(sbidx)/1e6) ' MHz']);
    [cal(antsel, sbidx), ~, ~] = statcal(R(antsel, antsel, sbidx), t_obs, fsel(sbidx), posITRFabs(antsel, :), srcsel, normal, 4, 20, eye(sum(antsel)));
end

%Apply calibration
antsel_mat = antsel* antsel';
% SJW: this should be a Hadamard product
Rsel = antsel_mat .* squeeze(R(:,:,sbidx));%select the R with sbidx
Cal_sel=cal(1:Nant*Nstat, sbidx);

Rcal = diag(Cal_sel)'*Rsel*diag(Cal_sel);

%Retry beamforming
Rcal_bf = zeros(6,6);
for i=1:6
    for j=1:6
        %stat1 = posxyz(1+(i-1)*48:(i*48),:);
        stat1=pos_stat(1+(i-1)*48:(i*48),:);
        tau1=(stat1*srcloc');
        % SJW: added sign for consistency with delay compensation in
        % imaging
        phs_tau1=exp(-2*pi*1i*fsel(sbidx)*tau1/c);
        
        %stat2=posxyz(1+(j-1)*48:(j*48),:);
        stat2=pos_stat(1+(j-1)*48:(j*48),:);
        tau2=(stat2*srcloc');
        % SJW: added minus sign, see above
        phs_tau2=exp(-2*pi*1i*fsel(sbidx)*tau2/c);
        Rcal_bf(i,j) = (phs_tau1'*Rcal(1+(i-1)*48:(i*48), 1+(j-1)*48:(j*48)))*phs_tau2;
    end
end
%make skyimage from the obtained beamformed covariance matrix that is now
%calibrated
figure
skymap_cal = acm2skyimage(Rcal_bf, stat_ref_local(:, 1), stat_ref_local(:, 2), fsel(sbidx), l, m);
pcolor(l, m, 10*log10((1/max(skymap_cal(:)))*skymap_cal))
xlabel('East← l →West');
ylabel('North← m →South');
shading flat
title('Beamformed Image towards Cas A with cal')
colorbar

%% verification by imaging the same area with antenna-based visibilities
skymap = acm2skyimage(Rcal, posxyz(:, 1), posxyz(:, 2), fsel(sbidx), l, m);
figure
pcolor(l, m, 10*log10((1/max(skymap(:)))*skymap))
xlabel('East← l →West');
ylabel('North← m →South');
title('Imaging Cas A with antenna-based visibilities')
shading flat
colorbar
